$(document).ready(function() {
  showHideSelect();
  navMenu();
});


function showHideSelect() {
  $('#test').on('change',  function() {
    $("#hidden_div1, #hidden_div2, #hidden_div3").css('display', 'none');
    if (this.selectedIndex == 0) $("#hidden_div1").css('display', 'block');
    if (this.selectedIndex == 1) $("#hidden_div2").css('display', 'block');
    if (this.selectedIndex == 2) $("#hidden_div3").css('display', 'block');
  });
}


function navMenu() {
	$('#header').prepend('<div id="menu-icon"><span class="first"></span><span class="second"></span><span class="third"></span></div>');
	$('#menu-icon').on('click', function(){
		$('nav').slideToggle();
		$(this).toggleClass('active');
	});
}